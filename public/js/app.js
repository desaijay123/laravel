var app = angular.module("app", ["ngRoute"]).config(function($routeProvider)
	{
		$routeProvider.when("/login",{
			templateUrl:"/templates/login.html",
			controller:"LoginController",

		});
		$routeProvider.when("/home", {
			templateUrl:"/templates/home.html",
			controller:"HomeController",
		});
		$routeProvider.otherwise({ redirectTo:"/login"});
	});

app.run(function($root))

app.factory("AuthenticationService", function($http, $location){
	return {
		login: function(credentials)
		{
			return $http.post("/auth/login", credentials);
		},
		logout:function()
		{
			return $http.get("/auth/logout");
		}
	};

});

app.controller("LoginController", function($scope, $location, AuthenticationService){
	$scope.credentials = { email:"", password:""};

	$scope.login = function()
	{
		AuthenticationService.login($scope.credentials).success(function(){
			$location.path("/home");	
		});
	}; 
});

app.controller("HomeController", function($scope, $location, AuthenticationService){
	$scope.title = "home";
	$scope.message = "hey jay";

	$scope.logout = function()
	{
		AuthenticationService.logout().success(function(){
			$location.path("/login");
		});
	};
});

app.directive("showsMessageWhenHovered", function(){
	return {
		restrict: "A",
		link: function(scope,element,attribute){
			var originalMessage = scope.message; 
			element.bind("mouseover", function(){
				scope.message = attribute.message;
				scope.$apply();
			});
			element.bind("mouseout", function(){
				scope.message = originalMessage;
				scope.$apply();
			});	
		}					 //A= attribute, C= class name, E=element, M=HTML comment
	};

});