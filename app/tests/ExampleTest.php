<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	 public function setUp()
	  
	  {
	    parent::setUp();

    Session::start();

    // Enable filters
    Route::enableFilters();
  	  }

  	  public function testSignin()
{
  Auth::shouldReceive('check')->once()->andReturn(false);
  $credentials = array(
        'official_email'=>'jay.desai@craftww.com',
        'password'=>'admin1234',
        'csrf_token' => csrf_token()
);

  $response = $this->action('POST', '/users/signin', $credentials); 

// if success user should be redirected to homepage
$this->assertRedirectedTo('apply/leave/leaveapp');
}

}
