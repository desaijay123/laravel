<?php

class Leavestatus extends \Eloquent
{
	use SoftDeletingTrait;

	protected $dates =["deleted_at"];
	protected $table = "leavestatus";


	public static $rules =  ["status" => "required|string|unique:leavestatus"];
}