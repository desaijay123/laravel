<?php

class Leavetype extends Eloquent
{
	use SoftDeletingTrait;

	protected $table = "leavetype";

	protected $dates = ['deleted_at'];

	protected $fillable = ["id" ,"leavetype"];

	public static $rules = ["leavetype"=>"required|unique:leavetype|string"];

	
}
