<?php

class Leaveapply extends Eloquent
{
	use SoftDeletingTrait;

	protected $dates =["deleted_at"];
	protected $table = "leaveapply";

	protected $fillable = ["id", "user_id", "leavetype_id", "from", "to", "created_at", "updated_at"];

	public static $rulesforapply = ["user_id"=>"integer",
								"leavetype_id" => "integer",
								"from" => "required",
								"to" => "required",
								"reason"=>"required",
								
								];
	public static $rulesfordatewiseleaves = ["from" =>"required|date",
											"to" => "required|date",
											];

	public function users()
	{
		return $this->belongsTo("User", "user_id");
	}


	 protected $status = array(
        '1' => 'Approved',
        '2' => 'Pending',
        '3' => "Viewed",
    );

    public function getStatusAttribute($value)
    {
        return $this->status[$value];
    }

//      public static function orderbydate($startDate,$endDate){

//     	ini_set('memory_limit', '-1');

//     	$oneday = true;

//     	if ($startDate != $endDate) $oneday = false;

//     	$today = date("Y-m-d");

//     	date_default_timezone_set("Asia/Calcutta");
//     	$today = strtotime($today);
// 		$startDate = strtotime($startDate);
// 		$endDate = strtotime("+1 day",strtotime($endDate));

// 		date_default_timezone_set("UTC");
// 		$today = date('Y-m-d H:i:s',$today);
// 		$startDate = date('Y-m-d H:i:s', $startDate);
// 		$endDate = date('Y-m-d H:i:s', $endDate);

//        $leavelisting = Leaveapply::whereBetween("created_at", array($startDate, $endDate))->get();

//        return $leavelisting;

// 			}
 }