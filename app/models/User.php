<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use SoftDeletingTrait;

	

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $dates =["deleted_at"];

	protected $fillable = ["id", "email","username" ,"password", "code", "type_id", "temp_password", "active", "remember_token","created_at", "updated_at"];

	public static $rules = [
						"official_email" =>"required|unique:users",
						"personal_email" =>"required|unique:users",
						"firstname" => "required",
						"lastname" =>"required",
						"address"=> "required",
						"personal_number" => "required|min:10|numeric",
						"emergency_number" => "required|min:10|numeric",
						"username"=>"required|unique:users|min:3",
						"password"=>"required|min:8|max:16",
						"confirmpassword"=>"required|same:password"]; 

	public static $rulesforlogin = ["official_email" => "required|email",
									"password" => "required"];

	public static $rulestoaddemployee = ["official_email" =>"required|unique:users",
						"personal_email" =>"required|unique:users",
						"firstname" => "required",
						"lastname" =>"required",
						"address"=> "required",
						"personal_number" => "required|min:10|numeric",
						"emergency_number" => "required|min:10|numeric",
						"username"=>"required|unique:users|min:3",
						"password"=>"required|min:8|max:16",
						"type" => "required"];

	public static $rulesforgotpassword = ["email" =>"required|email"];
	
	public static $rulesforchangingpassword = ["oldpassword"=>"required",
												"newpassword"=>"required|min:8|max:16",
												"confirmpassword"=>"required|same:newpassword"];

	public static $rulesforadmintoedittheprofile = ["email"=>"required|email|unique:users",
											"name" => "required|min:4",
											"username"=>"required|unique:users|min:3",];

	public static $rulesforupdatingusersrank = ["updaterank"=>"required"];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = array('password', 'remember_token');

	public function getAuthPassword() {
            return $this->password;
        }

	public function type()
	{
		return $this->hasOne("Type");
	}

	public function leaveapply()
	{
		return $this->hasMany("Leaveapply");
	}

	

	

}
