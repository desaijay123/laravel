<?php

class WorkFromHome extends Eloquent
{
	use SoftDeletingTrait;
    
    protected $dates =["deleted_at"];
	
	protected $table="work_from_home";

	protected $primary_key = "id"; 

	public static $rulestoapplyworkfromhome = ["user_id"=>"integer",
												"leavetype_id"=>"integer",
												"from"=>"required|date",
												"to"=>"required|date",
												"reason"=>"required|min:5",
												"hasil12" =>"integer"
												]; 
}