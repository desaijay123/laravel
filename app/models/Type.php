<?php

class Type extends Eloquent
{
	use SoftDeletingTrait;

	protected $table = "type";

	protected $dates = ["deleted_at"];

	protected $fillable = ["type"];

	public function User()
	{
		return $this->belongsTo("User");
	}

	public static $rulesforemployeetype = ["employeetype" =>"required|string|unique:type"];
}