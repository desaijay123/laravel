<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', array("as"=>"login", "uses"=>"AccountController@getSignin"));

// Route::post("/", array("as"=>"postlogin", "uses"=>"AccountController@postSignin"))->before("csrf");

// Route::get("/register", array("as"=>"user-register", "uses"=>"AccountController@getRegister"));

// Route::post("/register", array("as"=>"post-user-register", "uses"=>"AccountController@postRegister"));

// Route::get("/leaveapp", array("as"=>"leaveapp", "uses"=>"LeaveController@getLeaveapp"))->before("auth");

Route::controller("users", "AccountController");
Route::controller("admin", "AdminController");

Route::controller("apply/leave", "LeaveController");

Route::post("/admin/admineditprofile/{id}", array("as"=>"postadmineditprofile", "uses"=>"AdminController@Admineditprofile"));
