<?php
use Carbon\Carbon;
class AdminController extends BaseController
{
	public function __construct()
	{
    	$this->beforeFilter("csrf", array('on' => 'post'));
    	$this->beforeFilter("admin");
	}

	public function getLeavelisting()
	{
		if(Auth::check() && Auth::user()->admin == 1){

			$status = [];
			foreach(Leavestatus::all() as $state)
			{
				$status[$state->status] = $state->status;
			}
			return View::make("adminpanel.leavelisting")
				->with("leavelisting", Leaveapply::all())
				->with("status", $status);
			}
			elseif(!Auth::check()){
				return Redirect::to("users/signin");
			}
	}
	public function postTogglestatus()
	{
		$status = Leaveapply::find(strip_tags(stripslashes(Request::get("id"))));
		if($status)
		{
			$status->status_id =strip_tags(stripslashes(Request::get("status")));
			$status->save();

			return Redirect::to('admin/leavelisting')->withMessage("Status was updated");
		}

			return Redirect::to("admin/leavelisting")
					->withMessage("Invaild Product");

	}
	public function postTogglehomestatus()
	{
		$status = Workfromhome::find(strip_tags(stripslashes(Request::get("id"))));
		if($status)
		{
			$status->status_id =strip_tags(stripslashes(Request::get("status")));
			$status->save();

			return Redirect::to('admin/workfromhome')->withMessage("Status was updated");
		}

			return Redirect::to("admin/workfromhome")
					->withMessage("Invaild status");

	}
		public function postDestroyleavetype()
	{
		if(Auth::check())
		{
			$deleteleavetype = Leavetype::find(Request::get("id"));
			if($deleteleavetype)
			{
				$deleteleavetype->delete();
				return Redirect::to("admin/addleavetype")->withMessage("Leave type Deleted Succefully");
			}
			else
			{
				return Redirect::to("admin/addleavetype")->withMessage("Error Occured");
			}
		}
	}

	public function postDestroyemployee()
	{
		$deleteuser = User::find(strip_tags(stripslashes(Request::get("id"))));
		if($deleteuser)
		{
			$deleteuser->delete();
			return Redirect::to("admin/manageemployee");
		}
	}
	public function getExportleavedetails()
	{
		 $leave = Leaveapply::all();
    // the csv file with the first row
    $output = implode(",", array("id","name","leavetype","from","to","total days","reason", "Leave Taken on" ));
   	$output .="\n";
    foreach ($leave as $row) {
        // iterate over each row and add it to the csv
       
        $output .=  implode(",", array($row["id"], $row['name'],$row['leavetype_id'],$row['from'],$row['to'],$row['total_days'], $row["reason"], $row["created_at"])); // append each row
 		$output .="\n";
    }

    // headers used to make the file "downloadable", we set them manually
    // since we can't use Laravel's Response::download() function
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="leaveapplied.csv"',
        );

    // our response, this will be equivalent to your download() but
    // without using a local file
    return Response::make(rtrim($output, "\n"), 200, $headers);
	}

	public function getExporthomeleavedetails()
	{
		 $leave = Workfromhome::all();
    // the csv file with the first row
    $output = implode(",", array("id","name","leavetype","from","to","total days","reason", "Leave Taken on" ));
   	$output .="\n";
    foreach ($leave as $row) {
        // iterate over each row and add it to the csv
       
        $output .=  implode(",", array($row["id"], $row['name'],$row['leavetype_id'],$row['from'],$row['to'],$row['total_leaves'], $row["reason"], $row["created_at"])); // append each row
 		$output .="\n";
    }

    // headers used to make the file "downloadable", we set them manually
    // since we can't use Laravel's Response::download() function
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="workfromhomelist.csv"',
        );

    // our response, this will be equivalent to your download() but
    // without using a local file
    return Response::make(rtrim($output, "\n"), 200, $headers);
	}

	public function getAdmineditprofile($id)
		{
			$adminuser = User::findOrFail($id);
			
			if($adminuser)
			{
			return View::make("adminpanel.edituserprofile", compact("adminuser",$adminuser));
		}
		else
		{
			return Response::json("failed");
		}
	}

	public function postAdmineditprofile($id)
	{
		$adminuser = User::find($id);
			$validator = Validator::make(Request::all(), User::$rulesforadmintoedittheprofile);

			if($validator->fails())
			{
				return Redirect::to("admin/admineditprofile/$adminuser->id")->withErrors($validator)->withInput();
			}
			else
			{	$adminuser = User::find($id);
				$adminuser = User::where("id","=",$id);
				$adminuser = $adminuser->first();
				$adminuser = User::find(strip_tags(Request::get("id")));
				$adminuser->email = strip_tags(stripslashes(Request::get("email")));
				$adminuser->name  = strip_tags(stripslashes(Request::get("name")));
				$adminuser->username  = strip_tags(stripslashes(Request::get("username")));

				if($adminuser->save())
				{
					return Redirect::to("admin/admineditprofile/$adminuser->id")->withMessage("User Updated");
				}

			}
	}

	public function getAddemployee()
	{
		if(Auth::check() && Auth::user()->active == 1)
		{
			$type = [];
			foreach(Type::all() as $t)
			{
				$type[$t->id] = $t->employeetype;
			}
		
			return View::make("adminpanel.addemployee")
						->with("type", $type);
					
		}
		elseif(!Auth::check())
		{	
			return Redirect::to("users/signin");

		}
	}

	public function postAddemployee()
	{
		$validator = Validator::make(Request::all() , User::$rulestoaddemployee);

		if($validator->fails())
		{
			return Redirect::to("admin/addemployee")->withErrors($validator)->withInput();
		}
		else
		{

			$firstname =  strip_tags(stripslashes(Request::get("firstname")));
			$lastname  =  strip_tags(stripslashes(Request::get("lastname")));
			$address   =  strip_tags(stripslashes(Request::get("address")));
			$username  =  strip_tags(stripslashes(Request::get("username")));
			$personal_email  =  strip_tags(stripslashes(Request::get("personal_email")));
		    $official_email  =  strip_tags(stripslashes(Request::get("official_email")));
			$personal_number  =  strip_tags(stripslashes(Request::get("personal_number")));
			$emergency_number  =  strip_tags(stripslashes(Request::get("emergency_number")));
			$password  = strip_tags(stripslashes( Request::get("password")));
			$type = strip_tags(stripslashes(Request::get("type"))); 
			

			$employee = new User;
			$employee->firstname = $firstname;
			$employee->lastname = $lastname;
			$employee->personal_email = $personal_email;
			$employee->official_email = $official_email;
			$employee->address = $address;
			$employee->username = $username;
			$employee->personal_number = $personal_number;
			$employee->emergency_number = $emergency_number;
			$employee->password = Hash::make($password);
			$employee->type_id = $type;
			$employee->active = 1;
		
			if($employee->save())
			{
				return Redirect::to("admin/addemployee")->withMessage("1 employee added");
			}
		}
		return Redirect::to("admin/addemployee")->withMessage("Something went wrong");
	}

	public function getAddleavetype()
	{
		if(Auth::check())
		{
			return View::make("adminpanel.addleavetype")->with("leavetype", Leavetype::all());
		}
		else
		{
			return Redirect::to("/users/signin")->withMessage("Please signin to see this page");

		}
	}	
	public function postUpdateleavetype()
	{
		$validator = Validator::make(Request::all(), Leavetype::$rules);

		if($validator->fails())
		{
			return Redirect::to("admin/addleavetype")->withErrors($validator)->withInput();
		}
		elseif($validator->passes())

		{
			$addleavetype = new Leavetype;
			$addleavetype->leavetype = strip_tags(Request::get("leavetype"));

			if($addleavetype->save())
			{
				return Redirect::to("admin/addleavetype")->withMessage("Leave type added");
			}
			else
			{
			    return Redirect::to("admin/addleavetype")->withMessage("Something went wrong");

			}


		}

	}

	public function getAddemployeetype()
	{
		if(Auth::check())
		{
			return View::make("adminpanel.addemployeetype")->with("type", Type::all());
		}
		elseif (!Auth::check()) {
			return Redirect::to("/users/signin")->withMessage("Please sign in to View this page");
		}
	}

	public function postDestroy()
	{
			$delete = Type::find(strip_tags(Request::get('id')));
			if($delete){
			$delete->delete();
			return Redirect::to("admin/addemployeetype")->withMessage("Employee Type Deleted");
			
		}
	}

	public function postAddemployeetype()
	{
		$validator = Validator::make(Request::all(), Type::$rulesforemployeetype);

		if($validator->fails())
		{
			return Redirect::to("admin/addemployeetype")->withErrors($validator)->withInput();
		}
		elseif($validator->passes())
		{
			$addemployeetype = new Type;
			$addemployeetype->employeetype = strip_tags(Request::get("employeetype"));

			if($addemployeetype->save())
			{
				return Redirect::to("admin/addemployeetype")->withMessage("Employee type added");
			}
			else
			{
			    return Redirect::to("admin/addemployeetype")->withMessage("Something went wrong");
			}
		}

	}
	public function getExportuserdetails()
	
		{
			 $user = User::all();
	    // the csv file with the first row
	    $output = implode(",", array("id",'username', 'email'));
	   	$output .="\n";
	    foreach ($user as $row) {
	        // iterate over each row and add it to the csv
	       
	        $output .=  implode(",", array($row["id"],$row['username'], $row['email'])); // append each row
	 		$output .="\n";
	    }

	    // headers used to make the file "downloadable", we set them manually
	    // since we can't use Laravel's Response::download() function
	    $headers = array(
	        'Content-Type' => 'text/csv',
	        'Content-Disposition' => 'attachment; filename="users.csv"',
	        );

	    // our response, this will be equivalent to your download() but
	    // without using a local file
	    return Response::make(rtrim($output, "\n"), 200, $headers);
		}

	public function getManageemployee()
	{
		$updaterank = [];
		foreach(Type::all() as $rank)
		{	
			$updaterank[$rank->employeetype] = $rank->employeetype;
		}
		return View::make("adminpanel.manageemployee")
				->with("employees", User::all())
				->with("updaterank", $updaterank);
	}

	public function postUpdaterank()
	{
		$validator = Validator::make(Request::all(), User::$rulesforupdatingusersrank);

		if($validator->passes())
		{
			$updaterank = User::find(strip_tags(stripslashes(Request::get("id"))));

			if($updaterank)
			{
			$updaterank->type_id =strip_tags(stripslashes(Request::get("updaterank")));
			$updaterank->save();
		}
		return Redirect::to("admin/manageemployee")->withMessage("successfully updated rank");
		}
		elseif($validator->fails())
		{

		     return Redirect::to("admin/manageemployee")->withErrors($validator)->withInput();

		}

	} 

	public function getDatewiseleave()
	{
		$startDate = Input::get("from");
		$endDate = Input::get("to");


		$validator = Validator::make(Request::all(), Leaveapply::$rulesfordatewiseleaves);

		if(isset($startDate) && isset($endDate))
		{

		if($validator->fails())
		{
			return Redirect::to("admin/datewiseleave")->withErrors($validator)->withInput();
		}

		elseif($validator->passes())
		{ 	 	
		
    	ini_set('memory_limit', '-1');

    	$oneday = true;

    	if ($startDate != $endDate) $oneday = false;

    	$today = date("Y-m-d");

		date_default_timezone_set("UTC");
		$today = strtotime($today);
		$startDate =strtotime($startDate);
		$endDate =strtotime($endDate);
		
		date_default_timezone_set("Asia/Kolkata");
    	$today = date('Y-m-d H:i:s',$today);
		$startDate = date("Y-m-d H:i:s",$startDate);
		$endDate =   date('Y-m-d H:i:s',$endDate);

    	date_default_timezone_set("Asia/Kolkata");
    	$today = date($today);
		$startDate = new Datetime($startDate);
		$startDate  = $startDate->modify("-5 hours -30 minutes 00 seconds");
		$endDate =  new Datetime($endDate);
		$endDate  = $endDate->modify("+19 hours -30 minutes");
		// var_dump($startDate);
		// var_dump($endDate);

      $leavelisting = Leaveapply::where('created_at', ">=", $startDate)->where("created_at","<=",$endDate)->orderBy("created_at", "DESC")->get();
   }
}

   if(!isset($startDate) && !isset($endDate))
		{
		$leavelisting = Leaveapply::all();
		}
		return View::make("adminpanel.datewiseleave", compact("leavelisting", $leavelisting));

		
	}

	public function getWorkfromhome()
	{
		$workfromhomelist = Workfromhome::all();

		return View::make("adminpanel.workfromhomelist", compact("workfromhomelist", $workfromhomelist));
	}

	public function getAddleavestatustype()
	{
		$leavestatustype =  Leavestatus::all();

		return View::make("adminpanel.addleavestatustype", compact("leavestatustype", $leavestatustype));
	}

	public function postAddleavestatustype()
	{
		$validator = Validator::make(Request::all(), Leavestatus::$rules);

		if($validator->passes())
		{
			$leavestatustype = new Leavestatus;
			$leavestatustype->status = strip_tags(stripslashes(Request::get("status")));

			if($leavestatustype->save())
			{
				return Redirect::to("admin/addleavestatustype")->withMessage("Added the leave status");
			}
		}
		else
		{
			return Redirect::to("admin/addleavestatustype")->withErrors($validator)->withInput();
		}
	}
	public function postStatusdestroy()
	{
		$statusdestroy = Leavestatus::find(Request::get("id"));

		if($statusdestroy)
		{
			$statusdestroy->delete();
			return Redirect::to("admin/addleavestatustype")->withMessage("Leave Status Deleted");
		}
	}


	public function getReport()
	{
		// $cas = Input::get("casualleave");

		// if(isset($cas))
		// {
			$reported = Leaveapply::where("leavetype_id","=","Casual Leave")->orderBy("user_id","ASC")->groupBy("user_id")->get();
		// }
		return View::make("adminpanel.report", compact("reported","Casualleave"));

	}
	



}