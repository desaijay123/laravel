<?php


class AccountController extends BaseController
{
	public function __construct()
	{
		
		
		$this->beforeFilter("csrf", array('on' => 'post'));
	
	}
	
	public function getSignin()
	{
		return View::make("account.signin");
	}

	public function postSignin()
	{
		$validator =  Validator::make(Request::all(), User::$rulesforlogin);

		if($validator->fails())
		{
			return Redirect::to("users/signin")->withErrors($validator)->withInput();
		}
		else
		{
			$remember =(Request::has('rememberme')) ? true : false;
			//attempts sign in by user
			$auth = Auth::attempt(
				array(
				'official_email' => strip_tags(stripslashes(Request::get('official_email'))),
				'password'=>Request::get('password'),
				'active' =>1,
				), $remember);
			if($auth)
			{
				//redirect to the intended url 
				return Redirect::to('apply/leave/leaveapp')
								->withMessage('You are logged in');
			}
			else
			{
				return Redirect::to('/users/signin')
						->withMessage('Password/Email credentials are incorrect ');
			}
		}
		
		return Redirect::to('users/signin')
						->withMessage('There was a problem signing you');
	}

	public function getSignout()
	{
		Auth::logout();
		Session::flush();
		return Redirect::to("users/signin")->withMessage("You have signed out");
	}
	public function getRegister()
	{
		if(Auth::check())
		{
			return Redirect::to("apply/leave/leaveapp");

		}
		else
		{
		return View::make("account.register");
	}
	}

	public function postRegister()
	{
		$validator =  Validator::make(Request::all(), User::$rules);

		if($validator->fails())
		{
			return Redirect::to("users/register")->withErrors($validator)->withInput();
		}
		elseif($validator->passes())
		{
			$firstname =  strip_tags(stripslashes(Request::get("firstname")));
			$lastname  =  strip_tags(stripslashes(Request::get("lastname")));
			$address   =  strip_tags(stripslashes(Request::get("address")));
			$username  =  strip_tags(stripslashes(Request::get("username")));
			$personal_email  =  strip_tags(stripslashes(Request::get("personal_email")));
		    $official_email  =  strip_tags(stripslashes(Request::get("official_email")));
			$personal_number  =  strip_tags(stripslashes(Request::get("personal_number")));
			$emergency_number  =  strip_tags(stripslashes(Request::get("emergency_number")));
			$password  =  Request::get("password");
			$confirmpassword = Request::get("confirmpassword");

			
			$user = new User;
			$user->firstname = $firstname;
			$user->lastname = $lastname;
			$user->personal_email = $personal_email;
			$user->official_email = $official_email;
			$user->address = $address;
			$user->username = $username;
			$user->personal_number = $personal_number;
			$user->emergency_number = $emergency_number;
			$user->password = Hash::make($password);
			$user->active = 1;
			$user->type_id = 4;

			if ($user->save())
			{
				return Redirect::to("users/signin")->withMessage("You have been registered. Please Sign in!!");
			}
			else
			{
				return Redirect::to("users/register")->withMessage("Email/Password Mismatch");

			}	
		}
		return Redirect::to("users/register")->withMessage("Something went wrong ");



	}

	

	public function getForgotpassword()
	{
		if(!Auth::check())
		{
			return View::make("account.password");
		}
		elseif(Auth::check())
		{
			return Redirect::to("/apply/leave/leaveapply");
		}

	}

	public function postForgotpassword()
	{
		$validator = Validator::make(Request::all(), User::$rulesforgotpassword);

		if($validator->fails())
		{
			return Redirect::to("/users/forgotpassword")->withErrors($validator)->withInput();
		}

		else
		
		{
			//change password

			$user = User::where('email','=', Request::get('email'));
			if($user->count())
			{	
				$user= $user->first();
				
				//generate the code and the password
				$code = str_random(60);
				$password = str_random(10);
				
				$user->code = $code;			
				$user->temp_password = 	Hash::make($password);
			if($user->save())
			{
					Mail::send('emails.auth.forgot', array('link'=>URL::route('account-recover', $code), 'username'=>$user->username,'password'=>$password), function($message) use ($user) {
		 			$message->to($user->email, $user->username)->subject('Click to the link to change Your account password');
		 	});	
				return Redirect::to('/users/signin')->with('message','Password reminder sent! Please check your email for instructions on resetting your password.');
			}
		}
		return Redirect::to('/users/forgotpassword')->with('message','This email is not registered with us');

	}
}

	public function getRecover($code)
		{
			$user = User::where('code','=',$code)
						->where('temp_password','!=','');
			if($user->count())
			{
				$user= $user->first();
				$user->password = $user->temp_password;
				$user->temp_password = '';
				$user->code = '';
				if($user->save())
				{
					return Redirect::to('/users/signin')->withMessage('Your Password was recoverd and you can sign in with the new password provided you in the mail');
				}
			}
			return Redirect::to('/users/signin')->withMessage('Could not recover your account');
		}

		public function getChangepassword()
		{
			if(Auth::check())
			{
				return View::make("account.changepassword")
							->with("user", User::find(Auth::user()->id));
			}
			elseif(!Auth::check())
			{
				return Redirect::to("/users/signin")->withMessage("Please sign to change your password");
			}
		}

		public function postChangepassword()
		{
			if(Auth::check())

			{
				$validator = Validator::make(Request::all(), User::$rulesforchangingpassword);

				if($validator->passes())
				{
					$user = User::find(Auth::user()->id);
					$oldpassword = Request::get("oldpassword");
					$newpassword = Request::get("newpassword");
					$confirmpassword = Request::get("confirmpassword");

					if($oldpassword === $newpassword)
					{
						return Redirect::to('/users/changepassword')
									->with('message', 'Password must be differ from old password');

					}

					if(Hash::check($oldpassword, $user->getAuthpassword()))
					{
						$user->password = Hash::make($newpassword);

				if($user->save())
				{
					Auth::logout();
					Session::flush();
					
					return Redirect::to('/users/signin')
									->with('message', 'Your Password has been changed!! Please sign in with the new password');
				}
				
			}
			else
			{
				return Redirect::to("/users/changepassword")->withMessage("Old Password is not correct");
			}
			return Redirect::to('/users/changepassword')
						->with('message','Your old password is incorrect');
		}
				elseif($validator->fails())
				{
					return Redirect::to("/users/changepassword")->withInput()->withErrors($validator);

				}
		}
		else
		{
			return Redirect::to("/users/signin")
						->withMessage("Please sign in to change your password");
		}
	}






}