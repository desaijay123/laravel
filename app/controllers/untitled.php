<script>
var disabledDays = ["3-24-2015","3-25-2015"];

function nationalDays(date) {
  var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
 
  for (i = 0; i < disabledDays.length; i++) {
    if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 || new Date() > date) {
      return [false];
    }
  }
  return [true];
}
function noWeekendsOrHolidays(date) {
  var noWeekend = jQuery.datepicker.noWeekends(date);
  return noWeekend[0] ? nationalDays(date) : noWeekend;
}

 $(function() {
    $( "#from" ).datepicker({
        minDate:0,
        changeMonth: true,
        constrainInput: true,
         numberOfMonths: 1,
        beforeShowDay: noWeekendsOrHolidays,
        onSelect: calculateDays,
      onClose: function( selectedDate ) {
          var day = $("#from").datepicker('getDate');
       
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      },});
    });

    $(function() {
        $( "#to" ).datepicker({
            minDate:0,
            changeMonth: true,
            constrainInput: true,
             numberOfMonths: 1,

            beforeShowDay: noWeekendsOrHolidays,
            onSelect: calculateDays,
           onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      },});
        });

        function calculateDays(startDate, endDate) {
            var form = this.form

            var startDate = document.getElementById("from").value;
            var startDate = new Date(startDate);

            var endDate = document.getElementById("to").value;
            var endDate = new Date(endDate);

            startDate.setHours(0,0,0,1); // Start just after midnight
            endDate.setHours(23,59,59,999); // End just before midnight

            var oneDay = 24*60*60*1000;
            var diff = endDate - startDate; // Milliseconds between datetime objects
            var days = Math.ceil(diff / oneDay);

            var weeks = Math.floor(days / 7);
            var days = days - (weeks * 2);

            // Handle special cases
            var startDay = startDate.getDay();
            var endDay = endDate.getDay();

            // Remove weekend not previously removed.
            if(endDate < startDate)
            {
              return "errors";
            }
            if (startDay - endDay > 1 )
                days = days - 2;

            if (days)
                document.getElementById("hasil").innerHTML=days;
              $("#hasil").val(days);

             
        }

  </script>