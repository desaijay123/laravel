<?php

class LeaveController extends BaseController
{
	public function __construct()
	{
    	$this->beforeFilter("csrf", array('on' => 'post'));
	
	}

	public function getLeaveapp()
	{
		if(Auth::check())
		{
		return View::make("adminpanel.home");
	}
	else
	{
	 	return Redirect::to("/users/signin")->withMessage("Please sign in to see this view!!");
	}

	}	
	public function getWorkfromhome()
	{
		if(!Auth::check())	
		{
			return Redirect::to("/users/signin")->withMessage("Please signin to see this page");
		}
		elseif(Auth::check())
		{
			$leavetype = [];
			foreach(Leavetype::all() as $t)
			{
				$leavetype[$t->leavetype] = $t->leavetype;
			}
			return View::make("adminpanel.workfromhome")->with("leavetype", $leavetype);
		}
	}
	public function postWorkfromhome()
	{
		if(!Auth::check())	
		{
			return Redirect::to("/users/signin")->withMessage("Please signin to see this page");
		}
		elseif(Auth::check())
		{
			$validator = Validator::make(Request::all(), Workfromhome::$rulestoapplyworkfromhome);
		if($validator->passes())
		{
			$Workfromhome = new Workfromhome;
			$Workfromhome->user_id  = Auth::user()->id;
			$Workfromhome->name = e(Auth::user()->username);
			$Workfromhome->total_leaves = e(Request::get("hasil12"));
			$Workfromhome->leavetype_id = e(Request::get("leavetype"));
			$Workfromhome->from = e(Request::get("from"));
			$Workfromhome->to = e(Request::get("to"));
			$Workfromhome->reason = e(Request::get("reason"));
			$Workfromhome->status_id = 2;

			if($Workfromhome->save())
			{
				return Redirect::to("apply/leave/workfromhome")->withMessage("You have applied for leave");
			}
		}
		elseif($validator->fails())
		{
				return Redirect::to("apply/leave/workfromhome")->withErrors($validator)->withInput();
		}	
		}
	}
		
	public function getEmployeestatus()
	{
		if(Auth::check())
		{
	    $countleaves = Leaveapply::where("user_id","=",Auth::user()->id)->where("leavetype_id",'=',"Half Day")->sum('total_days');
	   $casual = Leaveapply::where("user_id","=",Auth::user()->id)->where("leavetype_id",'=',"Casual Leave")->sum('total_days');
	 $sick = Leaveapply::where("user_id","=",Auth::user()->id)->where("leavetype_id",'=',"Sick Leave")->sum('total_days');
  $totalleaves = $casual+$sick+$countleaves*0.5;
		$leaveapply = Leaveapply::where('user_id','=', Auth::user()->id)->paginate(10);

		$countleavestaken = 20 - $countleaves;
		return View::make('adminpanel.leavestatus', compact('leaveapply',"countleaves", 'countleavestaken','casual','totalleaves','sick'));		
		}
		else
		{
			return Redirect::to("users/signin")->withMessage("Please sign in to view this page !!");
		}
	}

	public function getApplyleave()
	{
		if(Auth::check())
		{
			Session::put("key", "value");
		$leavetype = [];
			foreach(Leavetype::all() as $t)
			{
				$leavetype[$t->leavetype] = $t->leavetype;
			}
		return View::make("adminpanel.applyleave")->with("leavetype", $leavetype);
	}
	elseif(!Auth::check())
	{
		return Redirect::to("/users/signin")->withMessage("Please sign in to see this view!!");
	}
	}
	public function postApplyleave()
	{
		$validator = Validator::make(Request::all(), Leaveapply::$rulesforapply);

		if($validator->passes())
		{
			$leaveapply = new Leaveapply;
			$leaveapply->user_id  = e(Auth::user()->id);
			$leaveapply->firstname = e(Auth::user()->firstname);
			$leaveapply->lastname = e(Auth::user()->lastname);
			$leaveapply->leavetype_id =strip_tags(stripslashes( Request::get("leavetype")));
			$leaveapply->from =strip_tags(stripslashes(Request::get("from")));
			$leaveapply->to = strip_tags(stripslashes( Request::get("to")));
			$leaveapply->total_days = strip_tags( stripslashes(Request::get("hasil")));
			$leaveapply->reason =strip_tags(stripslashes(Request::get("reason")));
			$leaveapply->status_id = 2;

			if($leaveapply->save())
			{
				return Redirect::to("apply/leave/applyleave")->withMessage("You have applied for leave");
			}
		}
		elseif($validator->fails())
		{
				return Redirect::to("apply/leave/applyleave")->withErrors($validator)->withInput();
		}
	}
	public function getWorkingfromhomestatus()
	{
		$homestatus = Workfromhome::where("user_id", "=", Auth::user()->id)->get();

		return View::make("adminpanel.workingfromhomestatus",compact("homestatus",$homestatus));
	}
}