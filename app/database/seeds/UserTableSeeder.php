<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        User::create(array('email' => 'desai.jay3@gmail.com',
        				   'name' =>"jay desai",
        				   "username"=>"desaijay",
        				   'admin'=>1,
        				   "active"=>1,
        				   "password"=> Hash::make("admin")));
    }

}