<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("users", function(Blueprint $table)
		{
			$table->increments("id");
			$table->string("official_email")->unique();
			$table->string("personal_email");
			$table->string("firstname");
			$table->string("lastname");
			$table->string("address");
			$table->string("username");
			$table->string("personal_number");
			$table->string("emergency_number");
			$table->string("password");
			$table->string("type_id");
			$table->string("code");
			$table->string("temp_password");
			$table->integer("active")->default(0);
			$table->integer("admin")->default(0);
			$table->string("remember_token");
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}


}
