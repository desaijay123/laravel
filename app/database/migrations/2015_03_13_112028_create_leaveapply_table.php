<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveapplyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("leaveapply", function(Blueprint $table)
		{
			$table->increments("id");
			$table->integer('user_id');
			$table->string("firstname");
			$table->string("lastname");
			$table->string("leavetype_id");
			$table->string("from");
			$table->string("to");
			$table->integer("total_days");
			$table->string("reason");
			$table->string("status_id");
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("leaveapply");
	}

}
