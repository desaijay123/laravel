<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkFromHomeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("work_from_home", function(Blueprint $table){
			$table->increments("id");
			$table->integer('user_id');
			$table->string("name");
			$table->string("leavetype_id");
			$table->string("from");
			$table->string("to");
			$table->string("reason");
			$table->string("total_leaves");
			$table->integer("status_id");
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("work_from_home");
	}

}