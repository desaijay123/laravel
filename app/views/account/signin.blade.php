@extends('layouts.main1')
@section('title', 'Login')
@section('content')
<div class="container">@if(Session::has("message"))<div class="col-md-4 col-md-offset-4">
    <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         {{Session::get("message")}}
        </div>
        </div>
        @endif
@if($errors->has())
    <div class='alert alert-danger' role='alert'><button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
  @endif<div id="spin-area"></div>
      <form class="form-signin" method="post" action="/users/signin">
        <h2 class="form-signin-heading">CRAFT LOGIN</h2>
        <hr class="colorgraph">
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="official" name="official_email"{{ (Input::old('official_email')) ? 'value="'. Input::old('official_email').'"':''}} class="form-control" placeholder="Email address" autofocus />
        <br />
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" value="" />
        <a class="pull-right" href="/users/forgotpassword">Forgot password?</a>
        <div class="checkbox" style="width:140px;">
              <label><input name="rememberme" id="rememberme" type="checkbox" value="Remember Me"> Remember Me</label>
            </div>
        <hr class="colorgraph">
        <button class="btn btn-lg btn-success btn-block" type="submit" name="submit"  onclick="makeitspin()"/>Sign In</button><br />
        <p class="text-center"><a href="/users/register">Register for an account?</a></p>
        {{ Form::token() }}
      </form>

    </div> <!-- /container -->
@stop
@section("footer")
<script>

var target = document.getElementById('spin-area');
function makeitspin(){
 var spinner = new Spinner({color:'#000000',direction:-1}).spin(target);
}

</script>
@stop