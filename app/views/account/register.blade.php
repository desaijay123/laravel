@extends('layouts.main1')
@section('content')
<div class="container">
@if(Session::has('message'))
{{Session::get('message')}}
@endif
<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    @if($errors->has())
		<div class='alert alert-danger' role='alert'><button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
			<p>The following errors have occured:</p>

			<ul>
				@foreach($errors->all() as $error)
				  		<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif
		<form role="form" action="/users/register" method="post">
			<h2>Please Sign Up <small>It's free and always will be.</small></h2>
			<hr class="colorgraph"><div id="spinner"></div>
			<div class="form-group">
				<input type="text" name="firstname"{{ (Input::old('firstname')) ? 'value="'.e(Input::old('firstname')).'"':'' }} id="firstname" class="form-control input-lg" placeholder="First Name" tabindex="3">
			</div>
			<div class="form-group">
				<input type="text" name="lastname"{{ (Input::old('lastname')) ? 'value="'.e(Input::old('lastname')).'"':'' }} id="lastname" class="form-control input-lg" placeholder="Last Name" tabindex="3">
			</div>
			<div class="form-group">
				<textarea  name="address"{{ (Input::old('address')) ? 'value="'.e(Input::old('address')).'"':'' }} id="address" class="form-control input-lg" placeholder="Address" tabindex="3"></textarea>
			</div>
			<div class="form-group">
				<input type="text" name="username"{{ (Input::old('username')) ? 'value="'.e(Input::old('username')).'"':'' }} id="username" class="form-control input-lg" placeholder="Display Name" tabindex="3">
			</div>
			<div class="form-group">
				<input type="email" name="official_email"{{ (Input::old('official_email')) ? 'value ="'. e(Input::old('official_email')).'"':'' }} id="official_email" class="form-control input-lg" placeholder="Official Email Address" tabindex="4">
			</div>
			<div class="form-group">
				<input type="email" name="personal_email"{{ (Input::old('personal_email')) ? 'value ="'. e(Input::old('personal_email')).'"':'' }} id="personal_email" class="form-control input-lg" placeholder="Personal Email Address" tabindex="4">
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="personal_number"{{ (Input::old('personal_number')) ? 'value ="'. e(Input::old('personal_number')).'"':'' }} id="personal_number" class="form-control input-lg" placeholder="Personal Number " tabindex="5">		
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="emergency_number"{{ (Input::old('emergency_number')) ? 'value ="'. e(Input::old('emergency_number')).'"':'' }} id="emergency_number" class="form-control input-lg" placeholder="Emergency Number" tabindex="6">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">		
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="confirmpassword" id="confirmpassword" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
					</div>
				</div>
			</div>
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6"><button type="submit"  class="btn btn-primary btn-block btn-lg" tabindex="7" onclick="togglespin()">Register</button></div>
				<div class="col-xs-12 col-md-6"><a href="/users/signin" class="btn btn-success btn-block btn-lg">Sign In</a></div>
			</div>
		{{Form::token() }}
		</form>
	</div>
</div>
</div>
@stop

@section("footer")
<script>
var target = document.getElementById("spinner");
var spinner;
var spinning = false;
function togglespin()
{
	spinning ? spinner.stop():spinner = new Spinner().spin(target);
	spinning != spinning;

}
</script>
@stop