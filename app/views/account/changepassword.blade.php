@extends('layouts.main1')
@section('content')
<div class="container">
<div class="row">@if(Session::has("message"))<div class="col-md-4 col-md-offset-4">
    <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         {{Session::get("message")}}
        </div>
        </div>
        @endif
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    @if($errors->has())
		<div class='alert alert-danger' role='alert'><button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
			<p>The following errors have occured:</p>

			<ul>
				@foreach($errors->all() as $error)
				  		<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif
		 <form class="form-signin" method="post" action="/users/changepassword">
        <h3 class="form-signin-heading">Change Your Password</h3>
        <hr class="colorgraph">
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="password" id="oldpassword" name="oldpassword" class="form-control" placeholder="Old Password" autofocus />
        <br />
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="newpassword" name="newpassword" class="form-control" placeholder="Password" value="" /> <br />
         <input type="password" id="confirmpassword" name="confirmpassword" class="form-control" placeholder="Confirm Password" value="" />
        <hr class="colorgraph">
        {{Form::hidden("id","$user->id")}}
        <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Change Password"/><br />
        {{ Form::token() }}
      </form>
	</div>
</div>


</div>
@stop