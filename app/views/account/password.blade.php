@extends('layouts.main1')
@section('content')
<div class="container">

<div class="row">
    <div class="col-md-4 col-md-offset-4"> @if(Session::has("message"))
    <div class="alert alert-success alert-dismissible">
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			   {{Session::get("message")}}
			  </div>
			  @endif
    @if($errors->has())
		<div class='alert alert-danger' role='alert'><button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
			<p>The following errors have occured:</p>

			<ul>
				@foreach($errors->all() as $error)
				  		<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif
		<form role="form" action="/users/forgotpassword" method="post">{{Form::token()}}
			<h2>Request Password Reset</h2>
			<hr class="colorgraph">
		
			<div class="form-group">
				<input type="email" name="email"{{ (Input::old('email')) ? 'value ="'. e(Input::old('email')).'"':'' }} id="email" class="form-control input-lg" placeholder="Email Address" tabindex="4">
			</div>
			<hr class="colorgraph">
			<p>
				<input class="btn btn-lg btn-success btn-block" type="submit" name ="submit" id="submit" value="Request password reset" />
			</p>
			<p class="text-center">Remembered your password? <a href="/users/signin">Login here</a></p>
		
		</form>
	</div>
</div>
</div>
@stop