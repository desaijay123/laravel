@include('layouts.partials.header')

<body>
@yield('content')
@include('layouts.partials.footer')
