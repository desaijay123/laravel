<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="jay desai, BeingJayDesai, Programmer, Hacker, Coding, Decoding,PHP,Software Engineer">
    <meta name="keyword" content="Web Developer, Designer, Magento, Php, Laravel, Coding, Web Development, Development Freelancer, Freelancing">
    <title>@yield('title')</title>

<style type="text/css">
  body{
    background-image: url('/img/looping-bg1.jpg');
  }
  </style>
	{{HTML::style('css/bootstrap.min.css')}}
	{{HTML::style('css/signin.css')}}
	{{HTML::style('css/signup.css')}}
  <style type="text/css">

#spin-area{
  margin-top:20px;
  height:0px;
  width:0px;
  background-color: #FFF;
  border-radius: 3px;
}

</style>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<nav class="navbar navbar-default navbar-static-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">CRAFT WORLDWIDE</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            </ul>
        </div>
      </div>
    </nav>
	</head>