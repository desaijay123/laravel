@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        @if(Auth::check())<strong> {{Auth::user()->username}}</strong>@endif Leave Details
                        <div class="pull-right">Total Leaves:<strong>20</strong> Half Day Taken:<strong>{{$countleaves*0.5}}</strong>
                         Casual Leave Taken:<strong>{{$casual}}</strong>  Sick Leaves:<strong>{{$sick}}</strong>
                          <strong>Total Leaves Taken: {{$totalleaves}}</strong>
                         </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <div class="table-responsive"> 
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-responsive table-bordered table-hover" id="leavestatus">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Leavetype</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Reason</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leaveapply as $l)@if(!empty($l))
                                        <tr class="odd gradeX">
                                            <td>{{$l->firstname}}</td>
                                            <td>{{$l->lastname}}</td>
                                            <td>{{$l->leavetype_id}}</td>
                                            <td>{{$l->from}}</td>
                                            <td>{{$l->to}}</td>
                                            <td>{{$l->reason}}</td>
                                            <td class="center"> <h5><span class="{{Availability::displayClass($l->status_id)}}">
                        {{ Availability::display($l->status_id) }}
                        </span>
                        </h5></td>@elseif(empty($l))
                                <h2>You haven't added any Leaves yet</h2>
                                @endif
                                        </tr>
                                  @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    @stop

    @section("footer")
     <script>
$(document).ready(function() {
    $('#leavestatus').DataTable();
} );

    </script>
    @stop