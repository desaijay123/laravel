@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           All Employees
                         <div class="pull-right"><form action="/admin/exportuserdetails" method="get">{{Form::token()}}<button class="btn btn-primary btn-info">Export User Details</button></form></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <div class="table-responsive"> 
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-responsive table-bordered table-hover" id="example">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>UserName</th>
                                            <th>Official Email</th>
                                            <th>Personal Email</th>
                                            <th>Personal Number</th>
                                            <th>Emergency Number</th>
                                            <th>Address</th>
                                            <th>Employees Rank</th>
                                            <th>Edit Employee</th>
                                            <th>Delete Employee</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees as $employee)
                                        <tr class="odd gradeX">
                                            <td>{{$employee->firstname}}</td>
                                            <td>{{$employee->lastname}}</td>
                                            <td>{{$employee->username}}</td>
                                            <td>{{$employee->official_email}}</td>
                                            <td>{{$employee->personal_email}}</td>
                                            <td>{{$employee->personal_number}}</td>
                                            <td>{{$employee->emergency_number}}</td>
                                            <td>{{$employee->address}}</td>
                                            <td>
                                            {{Form::open(array("url"=>"admin/updaterank", 'class'=>'form-inline')) }}
                                             {{ Form::hidden("id", $employee->id) }}
                                              {{ Form::select("updaterank", array("1"=>"VP", "2"=>"Sr.Digital Project Manager", "3"=>"Sr. Project Manager", "4"=>"Employee"), $employee->type_id) }}
                                            {{Form::submit("update", array("class"=>"btn btn-info"))}}                                           
                                            {{Form::close()}}
                                            </td>
                                            <td class="center"><a class="btn btn-primary btn-info" href="{{ URL::to('admin/admineditprofile/' . $employee->id) }}" target="_blank">Edit</a></td>
                                            {{ Form::hidden("id", $employee->id) }}
                                            <td class="center">
                                            {{ Form::open(array('url'=>'admin/destroyemployee', 'class'=>'form-inline','onsubmit' => 'return ConfirmDelete()')) }}
                                            {{ Form::hidden("id", $employee->id) }}
                                            {{ Form::submit('Delete', array("class"=>"btn btn-danger")) }}
                                            {{ Form::close() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

    @stop

    @section("footer")

    <script>

$(document).ready(function() {
    $('#example').DataTable();
} );

    </script>
    @stop