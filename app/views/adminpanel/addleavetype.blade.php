 @extends("adminpanel.home")
 @section("content")
 <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"></h1>
      </div>
      <!-- /.col-lg-12 --> 
    </div>
 <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> ADD LEAVE TYPE</div>
           @if($errors->has())
    <div class='alert alert-danger alert-dismissable' role='alert'> <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>

    </div> 
  @endif
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-6"><span class="error_message" id="message_sp" style="display:none;"> </span>
               @if(Auth::check())
               <form method="post" action="/admin/updateleavetype" role="form">{{Form::token()}}
                   <div class="form-group">
                      <input type="text" id="leavetype" name="leavetype">
                      <button type="submit" placeholder="add leave type" class="btn btn-info">Add Leave Type</button>
                      </div>
                </form>                    
                  @endif
               
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
          <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Delete</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leavetype as $t)
                                        <tr class="odd gradeX">
                                            <td>{{$t->leavetype}}</td>
                                            <td class="center">
                                            {{ Form::open(array('url'=>'admin/destroyleavetype', 'class'=>'form-inline','onsubmit' => 'return ConfirmDelete()')) }}
                                            {{ Form::hidden("id", $t->id) }}
                                            {{ Form::submit('Delete', array("class"=>"btn btn-danger")) }}
                                            {{ Form::close() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row --> 
    @stop

   