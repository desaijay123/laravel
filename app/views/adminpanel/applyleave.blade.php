 @extends("adminpanel.home")
 @section("content")
 <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">APPLY FOR LEAVE</h1>
      </div>
    </div>
 <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> APPLY LEAVE </div>
           @if($errors->has())
    <div class='alert alert-danger alert-dismissable' role='alert'> <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>
    </div> 
  @endif
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-6"> <span class="error_message" id="message_sp" style="display:none;"> </span>
                <form method="post" action="/apply/leave/applyleave" role="form">{{Form::token()}}
                   <div class="form-group">
                       <label for="Type">Type</label>:
                        <select name="leavetype">
                          <option value="Casual Leave">Casual Leave</option>
                          <option value="Paid Leave">Paid Leave</option>
                          <option value="Half Day">Half Day</option>
                          <option value="Sick Leave">Sick Leave</option>
                          <option value="Unplanned Leave">Unplanned Leave</option>
                        </select>                    
                  </div>
                  <div class="form-group">
                    <label>Leave From</label>
                    <input name="from" id="startDate" type="text" class="form-control"/ >
                  </div>
                  <div class="form-group" id="to_date">
                    <label>To</label>
                    <input name="to" id="endDate" type="text" class="form-control"  />
                  </div><input type="text" id="hasil" name="hasil" readonly/ >

                  <input type="hidden", id="todate">               
                  <div class="form-group">
                    <label>Reason</label>
                    <textarea name="reason" id="reason" type="text" class="form-control" rows='3'/></textarea>
                  </div><div id="spinner-area"></div>
                  <button name="submit" type="submit" class="btn btn-primary" id="calculateMe" onclick="spinnerarea()"/>Submit</button>
                  <!--<button type="submit" class="btn btn-default">Submit Button</button>-->
                  <input type="reset" name="reset" value="Reset" class="btn btn-default"/>
               
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row --> 
    @stop

   @section("footer")
 
   <script>
   /******
   * First, we'll extend the date object with some functionality.
   *   We'll add an  .each() function, as well as an .adjust() function.
   *   .each()  will give us the ability to loop between two dates, whether
   *     by 'day', 'week' or 'month'.
   *   .adjust() will allow us to move a given day by a given unit. This is used
   *     like so: currentDate.adjust('days', 1) to increment by one day.
   ******/
  Date.prototype.each = function(endDate, part, step, fn, bind){
    var fromDate = new Date(this.getTime()),
      toDate = new Date(endDate.getTime()),
      pm = fromDate <= toDate? 1:-1,
      i = 0;

    while( (pm === 1 && fromDate <= toDate) || (pm === -1 && fromDate >= toDate) ){
      if(fn.call(bind, fromDate, i, this) === false) break;
      i += step;
      fromDate.adjust(part, step*pm);
    }
    return this;
  };
  
  Date.prototype.adjust = function(part, amount){
  part = part.toLowerCase();
  
  var map = { 
        years: 'FullYear', months: 'Month', weeks: 'Hours', days: 'Hours', hours: 'Hours', 
        minutes: 'Minutes', seconds: 'Seconds', milliseconds: 'Milliseconds',
        utcyears: 'UTCFullYear', utcmonths: 'UTCMonth', weeks: 'UTCHours', utcdays: 'UTCHours', 
        utchours: 'UTCHours', utcminutes: 'UTCMinutes', utcseconds: 'UTCSeconds', utcmilliseconds: 'UTCMilliseconds'
      },
    mapPart = map[part];

  if(part == 'weeks' || part == 'utcweeks')
    amount *= 168;
  if(part == 'days' || part == 'utcdays')
    amount *= 24;
  
  this['set'+ mapPart]( this['get'+ mapPart]() + amount );

  return this;
}
  
  
/*******
 * An array of national holidays.
 ******/
natDays = [
  {
    month: 4,
    date: 1,
    type: "national",
    name: "New Year's Day"
  },
  {
    month: 4,
    date: 27,
    type: "national",
    name: "Martin Luther King Day"
  },
  {
    month: 2,
    date: 18,
    type: "national",
    name: "President's Day (Washington's Birthday"
  },
  {
    month: 5,
    date: 27,
    type: "national",
    name: "Memorial Day"
  },
  {
    month: 8,
    date: 15,
    type: "national",
    name: "Independence Day"
  },
  {
    month: 9,
    date: 2,
    type: "national - us",
    name: "Labor Day"
  },
  
  {
    month: 10,
    date: 14,
    type: "national - us",
    name: "Columbus Day"
  },
  {
    month: 11,
    date: 11,
    type: "national - us",
    name: "Veteran's Day"
  },
  {
    month: 11,
    date: 29,
    type: "national - us",
    name: "Thanksgiving Day"
  },
  {
    month: 12,
    date: 25,
    type: "national - us",
    name: "Christmas Day"
  }
  
];

/******
 * This uses the national holidays array we just set, and checks a given day to see
 *   if it's in the list. If so, it returns true and the name of the holiday, if not
 *   it returns false.
 *****/
function nationalDay(date) {
    for (i = 0; i < natDays.length; i++) {
      if (date.getMonth() == (natDays[i].month-1)
          && date.getDate() == natDays[i].date) {
        return [true, natDays[i].name];
      }
    }
  return [false, null];
}

/******
 * This function takes two dates, as start and end date, and iterates through the
 *   dates between them. For each date, it checks if the current date is a week day.
 *   If it is, it then checks if it isn't a holiday. In this case, it increments
 *   the business day counter.
 ******/
function calcBusinessDays(startDate, endDate) {
  // input given as Date objects
  var iDateDiff=0, holidays = [];
    
  startDate.each(endDate, 'days', 1, function(currentDate, currentStep, thisDate){
      if(currentDate.getDay() != 0 && currentDate.getDay() != 6 ) {
        var isAHoliday = nationalDay(currentDate);
          if(!isAHoliday[0]){
            iDateDiff += 1;
          } else {
            holidays.push(isAHoliday[1]);
          }
      }
   });
   return {count: iDateDiff, holidays: holidays};

};

$(function(){
  var results, exclusions;

$( "#startDate" ).datepicker({
      minDate:0,
      changeMonth: true,
      constrainInput: true,
      numberOfMonths: 1,
      beforeShowDay: $.datepicker.noWeekends,
      onClose: function( selectedDate ) {
        $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#endDate" ).datepicker({
      minDate:0,
      constrainInput: true,
      changeMonth: true,
      numberOfMonths: 1,
            beforeShowDay: $.datepicker.noWeekends,
      onClose: function( selectedDate ) {
        $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $("#calculateMe").on("click", function(){
      var startDate = new Date($("#startDate").val()),
          endDate = new Date($("#endDate").val() );
      // Calculate the number of business days. This returns an object, with
      //  two members: count and holidays
      results = calcBusinessDays(startDate, endDate);
      exclusions = "Excluded weekends";
      if (results.holidays.length > 0) {
        // We have holidays, tell the user about them...
        exclusions += " and the following holidays: ";
        for(var i=0; i<results.holidays.length; i += 1){
          exclusions += results.holidays[i]+", ";
        }
      } else {
        // No holidays.
        exclusions += ".";
      }
        var mysave = $('#hasil').text(results.count).append("<p>("+exclusions+")</p>");
if (mysave) document.getElementById("hasil").innerHTML = mysave;
                alert(results.count+" "+"Days"+" "+"Leave");

        $('#hasil').val(results.count);

    });
});
  </script>

  <script>
  var target = document.getElementById("spinner-area");
  function spinnerarea(){
 var spinner = new Spinner({color:'#000000',direction:-1}).spin(target);
}
</script>


@stop
