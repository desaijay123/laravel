 @extends("adminpanel.home")
 @section("content")
 <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header"></h1>
      </div>
      <!-- /.col-lg-12 --> 
    </div>
 <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> APPLY LEAVE AND WORK FROM HOME </div>
           @if($errors->has())
    <div class='alert alert-danger alert-dismissable' role='alert'> <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>

    </div> 
  @endif
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-6"> <span class="error_message" id="message_sp" style="display:none;"> </span>
                <form method="post" action="/apply/leave/workfromhome" role="form">{{Form::token()}}
                  <div class="form-group">
                       {{ Form::label("Type") }}:
                        {{ Form::select("leavetype", $leavetype ) }}
                                        
                  </div>

                 
                  <div class="form-group">
                    <label>Leave From</label>
                    <input name="from" id="from" type="text" class="form-control" value="" / >
                  </div>
                  <div class="form-group" id="to_date">
                    <label>To</label>
                    <input name="to" id="to" type="text" class="form-control" >
                  </div>
                  <input type="hidden" id="hasil12" name="hasil12" />
                  <input type="hidden" id="fromdate" name="fromdate"/>
                  <input type="hidden" id="todate" name="todate" />
                  <div class="form-group">
                    <label>Reason</label>
                    <textarea name="reason" id="reason" type="text" class="form-control" rows='3'/></textarea>
                  </div>
                  <input name="submit" type="submit" class="btn btn-primary" id="submit" value=" Submit " />
                  
                  <!--<button type="submit" class="btn btn-default">Submit Button</button>-->
                  <input type="reset" name="reset"  class="btn btn-danger"value="Reset" class="btn btn-default"/>
               
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row --> 
    @stop

    @section("footer")
 
   <script>
  var disabledDays = ["3-24-2015", "3-25-2015"];

   function nationalDays(date) {
       var m = date.getMonth(),
           d = date.getDate(),
           y = date.getFullYear();

       for (i = 0; i < disabledDays.length; i++) {
           if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1 || new Date() > date) {
               return [false];
           }
       }
       return [true];
   }

   function noWeekendsOrHolidays(date) {
       var noWeekend = jQuery.datepicker.noWeekends(date);
       return noWeekend[0] ? nationalDays(date) : noWeekend;
   }

   $(function () {
       $("#from").datepicker({
           minDate:0,
           changeMonth: true,
           constrainInput: true,
           numberOfMonths: 1,
           beforeShowDay: $.datepicker.noWeekends,
           onSelect: calculateDays,
           onClose: function (selectedDate) {
               var day = $("#from").datepicker('getDate');
               $("#to").datepicker("option", "minDate", selectedDate);
           },
       }).on("changeDate",function(ev){
      var fromdate = new Date(ev.date);
      fromdate.setMonth(fromdate.getMonth()+1);

      var finaldate = fromdate.getFullYear()+"-"+fromdate.getMonth()+"-"+fromdate.getDate();
      console.log(finaldate);
      $("#fromdate").val(finaldate);
   });

     });

   $(function () {
       $("#to").datepicker({
          minDate:0,
         beforeShowDay: $.datepicker.noWeekends,
           changeMonth: true,
           constrainInput: true,
           numberOfMonths: 1,
            
           onSelect: calculateDays,
           onClose: function (selectedDate) {
               $("#from").datepicker("option", "maxDate", selectedDate);
           },
       }).on("changeDate",function(ev){
      var todate = new Date(ev.date);
      todate.setMonth(todate.getMonth()+1);

      var finaldate = todate.getFullYear()+"-"+todate.getMonth()+"-"+todate.getDate();
      console.log(finaldate);
      $("#todate").val(finaldate);
   });

   });

   function calculateDays(startDate, endDate) {
       var form = this.form

       var startDate = document.getElementById("from").value;
       var startDate = new Date(startDate);

       var endDate = document.getElementById("to").value;
       var endDate = new Date(endDate);

       startDate.setHours(0, 0, 0, 1); // Start just after midnight
       endDate.setHours(23, 59, 59, 999); // End just before midnight

       var oneDay = 24 * 60 * 60 * 1000;
       var diff = endDate - startDate; // Milliseconds between datetime objects
       var days = Math.ceil(diff / oneDay);

       var weeks = Math.floor(days / 7);
       var days = days - (weeks * 2);

       // Handle special cases
       var startDay = startDate.getDay();
       var endDay = endDate.getDay();

       // Remove weekend not previously removed.
       if (endDate < startDate) {
           return 0;
       }

       if (startDay - endDay > 1) days = days - 2;

       if (days) document.getElementById("hasil12").innerHTML = days;
       $("#hasil12").val(days);

   }

  </script>

@stop