@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           All Employees Leave Details
                             @if($errors->has())
    <div class='alert alert-danger alert-dismissable' role='alert'> <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>

    </div> 
  @endif
                            <div class="pull-right"><form action="/admin/exportleavedetails" method="get">{{Form::token()}}<button class="btn btn-primary btn-info">Export Leave Details to csv</button></form></div>
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                   {{ Form::open(array('url' => '/admin/datewiseleave','method'=>'GET'))}}
<label for="from">From</label>
<input type="text" id="from"{{ (Input::old('from')) ? 'value="'.e(Input::old('from')).'"':'' }} name="from">
<label for="to">to</label>
<input type="text" id="to"{{(Input::old("to")) ?  'value ="'.e(Input::old('to')).'"':'' }} name="to">
<input type="hidden" id="fromdate" name="fromdate"/>
<input type="hidden" id="todate" name="todate" />
<button type="submit" class="btn btn-info" onclick="datetoggle()">Submit</button>
        {{ Form::close() }}
                        <br /><div id="datespinner"></div>
                                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Reason</th>
                                            <th>Date Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>@if(!empty($leavelisting))
                                    @foreach($leavelisting as $leavelist)
                                        <tr class="odd gradeX">
                                            <td>{{$leavelist->firstname}}</td>
                                            <td>{{$leavelist->from}}</td>
                                            <td>{{$leavelist->to}}</td>
                                            <td>{{$leavelist->reason}}</td>
                                            <td class="center">{{$leavelist->created_at}}</td>
                                        </tr>
                                  @endforeach
                                  @endif
                                    </tbody>
                                </table>
                            </div>@if(!empty($paginateValue))
                            {{$leavelisting->links()}}
                            @endif
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
          
    @stop

     @section("footer")
     <script>
var target = document.getElementById("datespinner");
function datetoggle()
{
  var spinner  = new Spinner({direction:-1, color:"#000000", lines:30,width:3}).spin(target);
}
</script>
   <script>
  $(function() {
    $( "#from" ).datepicker({

      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      numberOfMonths: 3,
      constraintInput: true,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      },
    }).on("changeDate",function(ev){
      var fromdate = new Date(ev.date);
      fromdate.setMonth(fromdate.getMonth()+1);

      var finaldate = fromdate.getFullYear()+"-"+fromdate.getMonth()+"-"+fromdate.getDate();
      console.log(finaldate);
      $("#fromdate").val(finaldate);
    });

    $( "#to" ).datepicker({
      minDate:1,
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      numberOfMonths: 3,
      constraintInput: true,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      },
    }).on("changeDate",function(ev){
      var todate = new Date(ev.date);
      todate.setDate(todate.getDate()+1);
      todate.setMonth(todate.getMonth()+1);

      var finaldate = todate.getFullYear()+"-"+todate.getMonth()+"-"+todate.getDate();
       console.log(finaldate);
      $("#todate").val(finaldate);
 
    })
  });
  </script>
<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable();
} );
</script>

@stop
