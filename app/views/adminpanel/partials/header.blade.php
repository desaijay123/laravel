<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Craft Worldwide Leave Management System</title>

    <!-- Bootstrap Core CSS -->
    {{ HTML::style("css/bootstrap.min.css") }}
         {{ HTML::style("css/normalize.css") }}
    {{HTML::style("css/metisMenu.min.css")}}
    {{ HTML::style("css/timeline.css") }}
    {{ HTML::style("css/sb-admin-2.css") }}
     {{ HTML::style("css/classic.css") }}
     {{ HTML::style("css/classic.date.css") }}
   
    {{ HTML::style("css/font-awesome.min.css") }}
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        

         <style>
            #cboxOverlay{ background:#666666; }
            .table-responsive
{
    overflow-x: auto;
}
        </style>

   
   <!-- end css -->

</head>