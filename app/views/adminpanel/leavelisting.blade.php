@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           All Employees Leave Details
                            <div class="pull-right"><form action="/admin/exportleavedetails" method="get">{{Form::token()}}<button class="btn btn-primary btn-info">Export Leave Details to csv</button></form></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                         <div class="table-responsive"> 
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-responsive table-bordered table-hover" id="leavelist">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Reason</th>
                                            <th>Leave Type</th>
                                            <th>Leaves Taken</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>@if(!empty($leavelisting))
                                    @foreach($leavelisting as $leavelist)
                                        <tr class="odd gradeX">
                                            <td>{{$leavelist->firstname}}</td>
                                            <td>{{$leavelist->lastname}}</td>
                                            <td>{{$leavelist->from}}</td>
                                            <td>{{$leavelist->to}}</td>
                                            <td>{{$leavelist->reason}}</td>
                                            <td>{{$leavelist->leavetype_id}}</td>
                                            <td>{{$leavelist->total_days}}</td>
                                            <td class="center">{{Form::open(array("url"=>"admin/togglestatus", "class"=>"form-inline")) }}
                        {{Form::hidden("id", $leavelist->id)}}
                        {{Form::select("status", array("1"=>"Approved", "2"=>"Pending", "3"=>"Viewed", "4" =>"onHold"), $leavelist->status_id)}}
                        {{Form::submit("Update", array("class"=>"btn btn-primary"))}}
                        {{Form::close()}}</td>
                                        </tr>
                                  @endforeach
                                  @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    @stop

@section("footer")
    <script>
$(document).ready(function() {
    $('#leavelist').DataTable();
} );

    </script>
@stop