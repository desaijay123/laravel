@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        @if(Auth::check())<strong> {{Auth::user()->username}}</strong>@endif Leave Details
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Leavetype</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Reason</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($homestatus as $home)
                                        <tr class="odd gradeX">
                                            <td>{{$home->name}}</td>
                                            <td>{{$home->leavetype_id}}</td>
                                            <td>{{$home->from}}</td>
                                            <td>{{$home->to}}</td>
                                            <td>{{$home->reason}}</td>
                                            <td class="center"> <h5> <span class="{{Availability::displayClass($home->status_id)}}">
                        {{ Availability::display($home->status_id) }}
                        </span>
                        </h5></td>
                                        </tr>
                                  @endforeach
                               
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           

    @stop