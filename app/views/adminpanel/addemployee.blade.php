@extends("adminpanel.home") 
@section("content")
 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">ADD EMPLOYEE(S) </h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
            
            <!-- /.row -->
            <div class="row"> @if($errors->has())
    <div class='alert alert-danger alert-dismissable' role='alert'> <button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
      <p>The following errors have occured:</p>

      <ul>
        @foreach($errors->all() as $error)
              <li>{{$error}}</li>
        @endforeach
      </ul>
    </div> 
  @endif
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            ADD EMPLOYEE
                        </div>
                        <div class="panel-body"><div id="ack"></div>
                            <div class="row">
                                <div class="col-lg-6">
                                <span class="error_message" id="message_sp" style="display:none;">  </span>
                                    <form id ="myform" method="post" action="/admin/addemployee" role="form">{{Form::token()}}          
                                    <div class="form-group">
                                            <label>First Name</label>
                                            <input name="firstname" {{ (Input::old('firstname')) ? 'value="'.e(Input::old('firstname')).'"':'' }}id="first_name1" type="text" class="form-control" value=""/ >
                                    </div>
                                     <div class="form-group">
                                            <label>Last Name</label>
                                            <input name="lastname" {{ (Input::old('lastname')) ? 'value="'.e(Input::old('lastname')).'"':'' }}id="lastname1" type="text" class="form-control" value=""/ >
                                    </div>
                                    <div class="form-group">
                                    {{ Form::label("Type") }}
                                    {{ Form::select("type", $type ) }}
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                            <label>Official Email</label>
									<input name="official_email" id="email"{{ (Input::old('official_email')) ? 'value="'.e(Input::old('official_email')).'"':'' }} type="text" class="form-control" value=""/ >
                                    </div>
                                     <div class="form-group">
                                            <label>Personal Email</label>
                                    <input name="personal_email" id="email"{{ (Input::old('personal_email')) ? 'value="'.e(Input::old('personal_email')).'"':'' }} type="text" class="form-control" value=""/ >
                                    </div>
                                
                                    	<div class="form-group">
                                            <label>Username</label>
									<input name="username"{{ (Input::old('username')) ? 'value="'.e(Input::old('username')).'"':'' }} id="username" type="text" class="form-control" value=""/ >
                                    </div>
                                    <div class="form-group">
                <textarea  name="address"{{ (Input::old('address')) ? 'value="'.e(Input::old('address')).'"':'' }} id="address" class="form-control input-lg" placeholder="Address" tabindex="3"></textarea>
            </div>
                                    <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="personal_number"{{ (Input::old('personal_number')) ? 'value ="'. e(Input::old('personal_number')).'"':'' }} id="personal_number" class="form-control input-lg" placeholder="Personal Number " tabindex="5">        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="emergency_number"{{ (Input::old('emergency_number')) ? 'value ="'. e(Input::old('emergency_number')).'"':'' }} id="emergency_number" class="form-control input-lg" placeholder="Emergency Number" tabindex="6">
                    </div>
                </div>
            </div>
          
          							
                                    <div class="form-group">
                                            <label>Password</label>
									<input name="password" id="password" type="password" class="form-control" />
                                    </div>
                                                
									<input name="submit" type="submit" class="btn btn-primary" id="submit" value="Submit"/>
                        
                                    <input type="reset" value=" Reset " id="button" class="btn btn-danger" name="Reset">   

                                   {{Form::close()}}
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            @stop 