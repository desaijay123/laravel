@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                    {{ Form::open(array('url' => '/admin/getReport','method'=>'GET'))}}
<label for="from">Users</label>
<input type="text" id="usersall"{{ (Input::old('usersall')) ? 'value="'.e(Input::old('usersall')).'"':'' }} name="usersall">
<label for="to">Leave Type</label>
<input type="text" id="leavetypeall"{{(Input::old("leavetypeall")) ?  'value ="'.e(Input::old('leavetypeall')).'"':'' }} name="leavetypeall">
<input type="submit" value="Submit" class="btn btn-info">
        {{ Form::close() }}
                        <div class="panel-heading">
                           All Employees Leave Report
                           </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                         <div class="table-responsive"> 
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-responsive table-bordered table-hover" id="report">
                                    <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Designation</th>
                                            <th>Total Leaves</th>
                                            <th>Casual Leave</th>
                                            <th>Sick Leave</th>
                                            <th>Half Day</th>
                                            <th>Unplanned Leave</th>
                                        </tr>
                                    </thead>
                                    <tbody> @foreach($reported as $r)
                                        <tr class="odd gradeX">
                                            <td>{{$r->firstname}}</td>
                                            <td>{{$r->lastname}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>@endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    @stop

@section("footer")
    <script>
$(document).ready(function() {
    $('#report').DataTable();
} );

    </script>
@stop