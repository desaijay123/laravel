@extends("adminpanel.home")
 @section("content")
          <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           All Employees Leave Details
                            <div class="pull-right"><form action="/admin/exporthomeleavedetails" method="get">{{Form::token()}}<button class="btn btn-primary btn-info">Export Leave Details to csv</button></form></div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover table-responsive" id="example123">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Leave Type</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Total Leaves</th>
                                            <th>Reason</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>@if(!empty($workfromhomelist))
                                    @foreach($workfromhomelist as $workfromhome)
                                        <tr class="odd gradeX">
                                            <td>{{$workfromhome->name}}</td>
                                            <td>{{$workfromhome->leavetype_id}}</td>
                                            <td>{{$workfromhome->from}}</td>
                                            <td>{{$workfromhome->to}}</td>
                                            <td>{{$workfromhome->total_leaves}}</td>
                                            <td>{{$workfromhome->reason}}</td>
                                            <td class="center">{{Form::open(array("url"=>"admin/togglehomestatus", "class"=>"form-inline")) }}
                        {{Form::hidden("id", $workfromhome->id)}}
                        {{Form::select("status", array("0"=>"select","1"=>"approved", "2"=>"pending", "3"=>"viewed"), $workfromhome->status_id)}}
                        {{Form::submit("Update", array("class"=>"btn btn-primary"))}}
                        {{Form::close()}}</td>
                                        </tr>
                                  @endforeach
                                  @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    @stop
    @section("footer")

    <script>

$(document).ready(function() {
    $('#example123').DataTable();
} );

    </script>
    @stop