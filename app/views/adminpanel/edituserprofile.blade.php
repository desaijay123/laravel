@extends('adminpanel.home')
@section('content')
<div class="container">

<div class="row">
 
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    @if($errors->has())
		<div class='alert alert-danger' role='alert'><button type="button" class="close" data-dismiss="alert" 
      aria-hidden="true">
      &times;
   </button>
			<p>The following errors have occured:</p>

			<ul>
				@foreach($errors->all() as $error)
				  		<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif
		{{Form::open(array("URL"=>'admin/admineditprofile', "PUT"))}}
			<h2>Update User Details</h2>
			<hr class="colorgraph">
			<div class="form-group">
				<input type="text" name="name"{{ (Input::old('name')) ? 'value="'.e(Input::old('name')).'"':'' }} value="{{{$adminuser->firstname}}}" id="name" class="form-control input-lg" placeholder="Full Name" tabindex="3">
			</div>
			<div class="form-group">
				<input type="text" name="username"{{ (Input::old('username')) ? 'value="'.e(Input::old('username')).'"':'' }}value="{{{$adminuser->lastname}}}" id="username" class="form-control input-lg" placeholder="Display Name" tabindex="3">
			</div>
			<div class="form-group">
				<input type="email" name="email"{{ (Input::old('email')) ? 'value ="'. e(Input::old('email')).'"':'' }} value="{{{$adminuser->official_email}}}"id="email" class="form-control input-lg" placeholder="Email Address" tabindex="4">
			</div>
			<hr class="colorgraph">
			<div class="row">{{Form::hidden("id", $adminuser->id)}}
				<div class="col-xs-12 col-md-6"><input type="submit" value="Update User Details" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>

				<div class="col-xs-12 col-md-6"><input type="reset" value="Reset" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
			</div>
		{{Form::close()}}
	</div>
</div>

</div>
@stop