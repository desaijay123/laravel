 @if(Auth::check())
@include('adminpanel.partials.header')
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">CRAFT WORLDWIDE |<strong>Hi {{Auth::user()->username}}</strong></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
               
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="/users/changepassword"><i class="glyphicon glyphicon-pencil"></i>Change Password</a>
                        <li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i>Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/users/signout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       @if(Auth::check() && Auth::user()->admin == 1)
                        <li>
                            <a href="/apply/leave/applyleave"><i class="fa fa-dashboard fa-fw"></i> Apply Leave</a>
                        </li>
                        <li>
                            <a href="/apply/leave/employeestatus"><i class="fa fa-table fa-fw"></i>Your Leave Status</a>
                        </li>
                         <li>
                            <a href="/apply/leave/workingfromhomestatus"><i class="fa fa-table fa-fw"></i>Leave Status if Working From Home</a>
                        </li>
                        <li>
                            <a href="/apply/leave/workfromhome"><i class="fa fa-table fa-fw"></i>Work From Home Requests</a>
                        </li>
                        <li>
                            <a href="/admin/addemployee"><i class="fa fa-table fa-fw"></i> Add Employee</a>
                        </li>
                        <li>
                            <a href="/admin/manageemployee"><i class="fa fa-table fa-fw"></i> Manage Employes</a>
                        </li>
                          <li>
                            <a href="/admin/leavelisting"><i class="fa fa-table fa-fw"></i> Leave Requests</a>
                        </li>
                        <li>
                            <a href="/admin/addleavetype"><i class="fa fa-table fa-fw"></i>Add Leave type</a>
                        </li>
                         <li>
                            <a href="/admin/addemployeetype"><i class="fa fa-table fa-fw"></i>Add Employee type</a>
                        </li>
                         <li>
                            <a href="/admin/datewiseleave"><i class="fa fa-table fa-fw"></i>Date Wise Leaves</a>
                        </li>
                         <li>
                            <a href="/admin/workfromhome"><i class="fa fa-table fa-fw"></i>People Working From Home</a>
                        </li>
                         <li>
                            <a href="/admin/addleavestatustype"><i class="fa fa-table fa-fw"></i>Add Leave Status</a>
                        </li>
                        @elseif(Auth::check() && Auth::user() && Auth::user()->admin != 1)
                        <li>
                            <a href="/apply/leave/applyleave"><i class="fa fa-dashboard fa-fw"></i> Apply Leave</a>
                        </li>
                        <li>
                            <a href="/apply/leave/employeestatus"><i class="fa fa-table fa-fw"></i>Your Leave Status</a>
                        </li>
                        <li>
                            <a href="/apply/leave/workfromhome"><i class="fa fa-table fa-fw"></i>Work From Home Requests</a>
                        </li>
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
        @if(Session::has("message"))<br /><div class="col-md-4 col-md-offset-4">
    <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         {{Session::get("message")}}
        </div>
        </div>
        @endif
           @yield("content")
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{HTML::script("js/jquery-1.11.2.min.js")}}
    {{HTML::script("js/bootstrap.min.js")}}
    {{HTML::script("js/metisMenu.min.js")}}
       {{HTML::script("js/sb-admin-2.js")}}
   <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
   <script src="{{ asset('js/picker.js') }}"></script>
   <script src="{{ asset('js/picker.date.js') }}"></script>
    <script src="{{ asset('js/picker.time.js') }}"></script>
    <script src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
     <script src="{{asset('js/spin.min.js')}}"></script>


   @yield("footer")

  

</body>

</html>
 @endif
